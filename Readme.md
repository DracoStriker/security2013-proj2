4th Year Course - Security
-------------------------------------------

Project Members
---------------
Simão Reis 

Rafael Saraiva

Project Description
-------------------
The goal of this project was to develop a flexible authentication mechanism for accessing a Linux system.  Robust authentication mechanisms such as the Citizen Card (CC) are not very convenient to be frequently used (e.g.several times per hour) and lack the proper interface with some classes of computing  devices  (e.g.,  smartphones  and  tablets).   On  the  other  hand, weak authentication mechanisms, such as long-term, password-based ones, are  more  convenient  for  many  platforms  classes  but  raise  several  security concerns.

In this project the objective was to develop a hybrid authentication framework, using both the Citizen Card and short lived passwords that can be mutated into one-time passwords. Furthermore, we used PAM (Pluggable Authentication Modules) to manage part of this framework.

General Architecture
--------------------
The  purpose  of  this  projet  was  to  develop  an  Hybrid  Authentication  Framework.  This framework allows a user, which has an account in a Linuxsystem, to setup a temporary account on the same system upon a successfulauthentication using the Citizen Card.

This setup is done remotely (through a secure web page); the target host runs specially developed server software for this purpose, and a client application dialogs both with the Citizen Card and the server in order to perform the card's owner authentication.

There is a policy for the authorization to create temporary accounts in the target host.  For example,  the server may get authorization instructions  from  a  central,  remote  authority,  or  have  them  locally.   The result is that different hosts may enforce different rules for their users,  in terms of authorization, password complexity, or duration of the temporary passwords.

Authentication of the Temporary Accounts
----------------------------------------
The temporary accounts have a short, PIN-like password.  The password is as random as possible,  different for each instantiation of the temporary account, and should not be provided by the user (i.e., they must be generated by the software).  Simultaneously, the password is easy to memorize.

Temporary accounts are short-lived. The specific duration is a configuration detail provided by the authorization policies used when creating temporary accounts.

Password Expiration Rules
-------------------------
During the account lifetime its owner can use the password provided upon the account instantiation as many times as it wants, provided that the password is conveyed for verification through a secure communication channel, and  no  attacks  are  suspected.   Therefore,  two  rules  restricting  password expiration were implemented:

* If  conveyed  otherwise  through  an  insecure  communication  channel (e.g.,  in  a  telnet  login),  the  password  should  there after  be  considered a one-time password, which means that after that authenticationit can no longer be used for that account.
* If invalid passwords are provided for an account, the password is also considered to be expired.

When the password for the temporary account is considered to be expired, access to the temporary account is forbidden until a successful authentication with the Citizen Card is made by the account owner.

Extra security features
-----------------------
Capability to restrict logins from the temporary account to particular geographic areas.

Implementation
--------------
* Apache2 Web Server;
* SQLite3;
* The Portuguese Citizen's Card (Smart cards);
* Custom PAM module;
* GeoIP;
